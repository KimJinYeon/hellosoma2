class Hello:

    def say(self):
        print('hello, world')

    def add(self, x, y):
        res = x+y
        print("x+y=", (x + y))
        return res


if __name__ == "__main__":
    h = Hello()
    h.say()
    h.add(2, 3)
