import hello
import unittest


class TestHello(unittest.TestCase):
    def test_add(self):
        h = hello.Hello()
        result = h.add(2, 3)
        self.assertEqual(result, 5)


if __name__ == "__main__":
    unittest.main()
